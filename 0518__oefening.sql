USE modernWays;

ALTER TABLE huisdieren ADD COLUMN geluid CHAR (20);
SET SQL_SAFE_UPDATES = 0;
UPDATE huisdieren
SET geluid='WAF'
WHERE Soort = 'hond';
UPDATE huisdieren
SET geluid='Miauww'
WHERE Soort='kat';
SET SQL_SAFE_UPDATES = 1;